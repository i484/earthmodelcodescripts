#!/bin/sh
# This script prepares WFDE5 dataset for use in CLM5.

module load CDO # Load CDO (Climate data operator). Use command only if you are working on an HPC

# Iterate through files using the dates on them and reproduce the netcdf file on regular grid.
export da=2000-01-01
while [ "$da" != '2004-01-01' ] ; do

        #echo $da
	da="$(date -u --date "$da" '+%Y-%m-%d')"
	yr="$(date -u --date "$da" '+%Y')"
  	mn="$(date -u --date "$da"  '+%m')"
	da="$(date -u --date "$da + 1 month" '+%Y-%m-%d')"
	echo "Preparing $yr$mn Input"

	ncks -O --msa -d lon,0.25,179.75 -d lon,-179.75,-0.25 Rainf_WFDE5_CRU_${yr}${mn}_v1.0.nc reordered${yr}${mn}.nc # Convert longitude dimension from 0-360 to -180 to 180
	ncap2 -O -s 'where(lon < 0) lon=lon+360' reordered${yr}${mn}.nc rotated${yr}${mn}.nc
	cdo -v setmisstonn rotated${yr}${mn}.nc filled${yr}${mn}.nc #fill missing values with nearest neighbour

	
	cat > lonlat2generic.py << EOF
import numpy as np
from netCDF4 import Dataset
#import sys
#import pylab as plt


filename = 'filled${yr}${mn}.nc'

f1 = Dataset(filename, 'r')

# extraction of the data from your file
lonvar = f1.variables['lon'][:]
latvar = f1.variables['lat'][:]
Rainvar = f1.variables['Rainf'][:]
#Vvar = f1.variables['V'][:]

# extraction of time variable
timevar = f1.variables['time'][:]

# extraction of U dimensions
# /!\ Warning /!\
# In your file, the dimensions are (time, lat, lon) and not (time, lon, lat)
ntime, nlat, nlon = Rainvar.shape

f1.close()

# opening corrected dataset
f = Dataset('n${yr}-${mn}.nc', 'w', format='NETCDF4_CLASSIC')

# creation of dimensions
time = f.createDimension('time', ntime)
lon = f.createDimension('lon', nlon)
lat = f.createDimension('lat', nlat)
scalar = f.createDimension('scalar', 1)

# creation of variables
time = f.createVariable('time', np.float64, ('time',))
lon = f.createVariable('LONGXY', np.float32, ('lon'))
lat = f.createVariable('LATIXY', np.float32, ('lat',))
Rainf = f.createVariable('Rainf', 'float64', ('time', 'lat', 'lon'))
EDGEE = f.createVariable('EDGEE', 'float64', ('scalar'))
EDGEW = f.createVariable('EDGEW', 'float64', ('scalar'))
EDGES = f.createVariable('EDGES', 'float64', ('scalar'))
EDGEN = f.createVariable('EDGEN', 'float64', ('scalar'))

# saving the variables
time[:] = timevar
lon[:] = lonvar
lat[:] = latvar
EDGEE[:] = 360
EDGEW[:] = 0
EDGEN[:] = 90
EDGES[:] = -90
Rainf[:] = Rainvar


f.close()

quit()

EOF
    	
    	python lonlat2generic.py
	cdo -v setreftime,1900-01-01,0,hours n${yr}-${mn}.nc nn${yr}-${mn}.nc #Set reference time
	cdo -v settaxis,${yr}-${mn}-01,00:00:00,1hour nn${yr}-${mn}.nc nnn${yr}-${mn}.nc # Set time axis
	cdo setcalendar,proleptic_gregorian nnn${yr}-${mn}.nc nnnn${yr}-${mn}.nc #Convert calendear to proleptic gregorian
  	
  	rm Rainf_WFDE5_CRU_${yr}${mn}_v1.0.nc # Remove temporary files
	rm n${yr}-${mn}.nc
	rm nn${yr}-${mn}.nc
        rm nnn${yr}-${mn}.nc
	mv nnnn${yr}-${mn}.nc ${yr}-${mn}.nc

done

rm reordered*.nc
rm filled*.nc
rm rotated*.nc



#!/bin/sh
module load CDO
cd SWdown1Hrly/

export da=2000-01-01
while [ "$da" != '2004-01-01' ] ; do

        #echo $da
	da="$(date -u --date "$da" '+%Y-%m-%d')"
	yr="$(date -u --date "$da" '+%Y')"
  	mn="$(date -u --date "$da"  '+%m')"
	da="$(date -u --date "$da + 1 month" '+%Y-%m-%d')"
	echo "Preparing $yr$mn Input"

	ncks -O --msa -d lon,0.25,179.75 -d lon,-179.75,-0.25 SWdown_WFDE5_CRU_${yr}${mn}_v1.0.nc reordered${yr}${mn}.nc
	ncap2 -O -s 'where(lon < 0) lon=lon+360' reordered${yr}${mn}.nc rotated${yr}${mn}.nc
	cdo -v setmisstonn rotated${yr}${mn}.nc filled${yr}${mn}.nc

	
	cat > lonlat2generic.py << EOF
import numpy as np
from netCDF4 import Dataset
#import sys
#import pylab as plt


filename = 'filled${yr}${mn}.nc'

f1 = Dataset(filename, 'r')

# extraction of the data from your file
lonvar = f1.variables['lon'][:]
latvar = f1.variables['lat'][:]
SWvar = f1.variables['SWdown'][:]
#Vvar = f1.variables['V'][:]

# extraction of time variable
timevar = f1.variables['time'][:]

# extraction of U dimensions
# /!\ Warning /!\
# In your file, the dimensions are (time, lat, lon) and not (time, lon, lat)
ntime, nlat, nlon = SWvar.shape

f1.close()

# opening corrected dataset
f = Dataset('n${yr}-${mn}.nc', 'w', format='NETCDF4_CLASSIC')

# creation of dimensions
time = f.createDimension('time', ntime)
lon = f.createDimension('lon', nlon)
lat = f.createDimension('lat', nlat)
scalar = f.createDimension('scalar', 1)

# creation of variables
time = f.createVariable('time', np.float64, ('time',))
lon = f.createVariable('LONGXY', np.float32, ('lon'))
lat = f.createVariable('LATIXY', np.float32, ('lat',))
SWdown = f.createVariable('SWdown', 'float64', ('time', 'lat', 'lon'))
EDGEE = f.createVariable('EDGEE', 'float64', ('scalar'))
EDGEW = f.createVariable('EDGEW', 'float64', ('scalar'))
EDGES = f.createVariable('EDGES', 'float64', ('scalar'))
EDGEN = f.createVariable('EDGEN', 'float64', ('scalar'))

# saving the variables
time[:] = timevar
lon[:] = lonvar
lat[:] = latvar
EDGEE[:] = 360
EDGEW[:] = 0
EDGEN[:] = 90
EDGES[:] = -90
SWdown[:] = SWvar


f.close()

quit()

EOF
    	
    	python lonlat2generic.py
	cdo -v setreftime,1900-01-01,0,hours n${yr}-${mn}.nc nn${yr}-${mn}.nc
	cdo -v settaxis,${yr}-${mn}-01,00:00:00,1hour nn${yr}-${mn}.nc nnn${yr}-${mn}.nc
	cdo setcalendar,proleptic_gregorian nnn${yr}-${mn}.nc nnnn${yr}-${mn}.nc
  	
  	rm SWdown_WFDE5_CRU_${yr}${mn}_v1.0.nc
	rm n${yr}-${mn}.nc
	rm nn${yr}-${mn}.nc
        rm nnn${yr}-${mn}.nc
	mv nnnn${yr}-${mn}.nc ${yr}-${mn}.nc
        
done
rm reordered*.nc
rm rotated*.nc
rm filled*.nc

#!/bin/sh
module load CDO
cd TPQLW1Hrly/

export da=2000-01-01
while [ "$da" != '2004-01-01' ] ; do

        #echo $da
	da="$(date -u --date "$da" '+%Y-%m-%d')"
	yr="$(date -u --date "$da" '+%Y')"
  	mn="$(date -u --date "$da"  '+%m')"
	da="$(date -u --date "$da + 1 month" '+%Y-%m-%d')"
	echo "Preparing $yr$mn Input"

	ncks -O --msa -d lon,0.25,179.75 -d lon,-179.75,-0.25 TPQLW_WFDE5_CRU_${yr}${mn}_v1.0.nc reordered${yr}${mn}.nc
	ncap2 -O -s 'where(lon < 0) lon=lon+360' reordered${yr}${mn}.nc rotated${yr}${mn}.nc
	cdo -v setmisstonn rotated${yr}${mn}.nc filled${yr}${mn}.nc

	
	cat > lonlat2generic.py << EOF
import numpy as np
from netCDF4 import Dataset
#import sys
#import pylab as plt


filename = 'filled${yr}${mn}.nc'

f1 = Dataset(filename, 'r')

# extraction of the data from your file
lonvar = f1.variables['lon'][:]
latvar = f1.variables['lat'][:]
Tvar = f1.variables['Tair'][:]
Qvar = f1.variables['Qair'][:]
Pvar = f1.variables['PSurf'][:]
Wvar = f1.variables['Wind'][:]
Lvar = f1.variables['LWdown'][:]


# extraction of time variable
timevar = f1.variables['time'][:]

# extraction of U dimensions
# /!\ Warning /!\
# In your file, the dimensions are (time, lat, lon) and not (time, lon, lat)
ntime, nlat, nlon = Tvar.shape
ntime, nlat, nlon = Qvar.shape
ntime, nlat, nlon = Pvar.shape
ntime, nlat, nlon = Wvar.shape
ntime, nlat, nlon = Lvar.shape

f1.close()

# opening corrected dataset
f = Dataset('n${yr}-${mn}.nc', 'w', format='NETCDF4_CLASSIC')

# creation of dimensions
time = f.createDimension('time', ntime)
lon = f.createDimension('lon', nlon)
lat = f.createDimension('lat', nlat)
scalar = f.createDimension('scalar', 1)

# creation of variables
time = f.createVariable('time', np.float64, ('time',))
lon = f.createVariable('LONGXY', np.float32, ('lon'))
lat = f.createVariable('LATIXY', np.float32, ('lat',))
Tair = f.createVariable('Tair', 'float64', ('time', 'lat', 'lon'))
PSurf = f.createVariable('PSurf', 'float64', ('time', 'lat', 'lon'))
Qair = f.createVariable('Qair', 'float64', ('time', 'lat', 'lon'))
Wind = f.createVariable('Wind', 'float64', ('time', 'lat', 'lon'))
LWdown = f.createVariable('LWdown', 'float64', ('time', 'lat', 'lon'))

EDGEE = f.createVariable('EDGEE', 'float64', ('scalar'))
EDGEW = f.createVariable('EDGEW', 'float64', ('scalar'))
EDGES = f.createVariable('EDGES', 'float64', ('scalar'))
EDGEN = f.createVariable('EDGEN', 'float64', ('scalar'))

# saving the variables
time[:] = timevar
lon[:] = lonvar
lat[:] = latvar
EDGEE[:] = 360
EDGEW[:] = 0
EDGEN[:] = 90
EDGES[:] = -90
Tair[:] = Tvar
PSurf[:] = Pvar
Qair[:] = Qvar
Wind[:] = Wvar
LWdown[:] = Lvar


f.close()

quit()



EOF
    	
    	python lonlat2generic.py
	cdo -v setreftime,1900-01-01,0,hours n${yr}-${mn}.nc nn${yr}-${mn}.nc
	cdo -v settaxis,${yr}-${mn}-01,00:00:00,1hour nn${yr}-${mn}.nc nnn${yr}-${mn}.nc
	cdo setcalendar,proleptic_gregorian nnn${yr}-${mn}.nc nnnn${yr}-${mn}.nc
  	
  	rm TPQLW_WFDE5_CRU_${yr}${mn}_v1.0.nc
	rm n${yr}-${mn}.nc
	rm nn${yr}-${mn}.nc
        rm nnn${yr}-${mn}.nc
	mv nnnn${yr}-${mn}.nc ${yr}-${mn}.nc

done

rm reordered*.nc
rm rotated*.nc
rm filled*.nc


